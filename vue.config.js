const path = require('path');

module.exports = {
  outputDir: path.resolve(__dirname, 'dist'),
  assetsDir: 'assets',
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8000',
      },
      '/badge': {
        target: 'http://localhost:8000',
      },
    },
  },


};
