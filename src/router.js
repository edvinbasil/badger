import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Projects from './views/Projects.vue';
import Project from './views/Project.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/:service/:name',
      name: 'projects',
      component: Projects,
    },
    {
      path: '/:service/:name/:project',
      name: 'project',
      component: Project,
    },
    // component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
  ],
});
