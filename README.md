# Badger

> npm dependency, version and license badge generator for github and gitlab projects.

## Usage

- Start by going to [badger.edvinbasil.com](hnttps://badger.edvinbasil.com)
- Enter your username and select your service.  
  Supported services are:
  - Gitlab
  - Github
- You will get a list of your public projects
- Choose an npm project to view its dependencies
- You can copy the badges to use in your own projects

![](preview.png)