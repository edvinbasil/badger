const badgen = require('badgen')
async function getBadge (status='invalid', type) {
  let color = ''
  switch (status) {
    case 'outofdate':
    case 'insecure':
      color = 'e05d44'
      break
    case 'unknown':
      color = '9f9f9f'
      break
    case 'up to date':
      color = '44cc11' //rgb(223, 179, 23)
      break
    default:
      color = 'dfb317'
  }
  switch(type) {
    case 'version':
      color = 'blue'
      break
    case 'license':
      color = 'yellow'
      break
  }

  // return badge({ text: [type, status], colors: { right: color } })
  return badgen({
    subject: type,
    status,
    color,
    style: 'flat'
  })
}

module.exports = {
  getBadge
}
